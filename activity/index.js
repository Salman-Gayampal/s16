//console.log("hello");
/*
	1. Debug the following code to return the correct value and mimic the output.
*/

let num1 = 25;
let num2 = 5;
console.log("The result of num1 + num2 should be 30.");
console.log("Actual Result:");
console.log(num1 + num2);

let num3 = 156;
let num4 = 44;
console.log("The result of num3 + num4 should be 200.");
console.log("Actual Result:");
console.log(num3 + num4);

let num5 = 17;
let num6 = 10;
console.log("The result of num5 - num6 should be 7.");
console.log("Actual Result:");
console.log(num5 - num6);
  
/*

2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.

*/
let minutesHour = 60;
let hoursDay = 24;
let daysWeek = 7;
let weeksMonth = 4;
let monthsYear = 12;
let daysYear = 365;

let resultMinutes = minutesHour * hoursDay * daysYear;
  console.log('There are ' + resultMinutes + ' minutes in a year.');

/*
3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
*/
let tempCelsius = 132;
let resultFahrenheit = (tempCelsius * (9/5)) + 32;

console.log(tempCelsius + ' degrees Celsius when converted to Farenheit is ' + resultFahrenheit)

/*
4a. Given the values below, identify if the values of the following variable are divisible by 8.
   -Use a modulo operator to identify the divisibility of the number to 8.
   -Save the result of the operation in an appropriately named variable.
   -Log the value of the remainder in the console.
   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
   -Log a message in the console if num7 is divisible by 8.
   -Log the value of isDivisibleBy8 in the console.

*/
let num7 = 165;
let num7Modulo = num7 % 8;
let isDivisibleBy8 = num7Modulo === 0;

//Log the value of the remainder in the console.
console.log('The remainder of ' + num7 + ' divided by 8 is: ' + num7Modulo)
console.log("Is num7 divisible by 8?");
//Log the value of isDivisibleBy8 in the console.
console.log(isDivisibleBy8)

/*
4b. Given the values below, identify if the values of the following variable are divisible by 4.
   -Use a modulo operator to identify the divisibility of the number to 4.
   -Save the result of the operation in an appropriately named variable.
   -Log the value of the remainder in the console.
   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
   -Log a message in the console if num8 is divisible by 4.
   -Log the value of isDivisibleBy4 in the console.

*/
let num8 = 348;
let num8Modulo = 348 % 4;
let isDivisibleBy4 = num8Modulo === 0;
//Log the value of the remainder in the console.
console.log('The remainder of ' + num8 + ' divided by 4 is: ' + num8Modulo)
console.log("Is num8 divisible by 4?");
//Log the value of isDivisibleBy4 in the console.
console.log(isDivisibleBy4);


/* 
Activity:
1. In the S16 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code from Boodle Notes and paste it into your index.js for the code and instructions.
4. Debug the given code to return the correct value and mimic the output. 
  Check the value’s data type.
  Check the if the operator used is correct.
5. Given the values in the code, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.
  Log the result in the console.
6. Given the values in the code, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
  Log the value of the variable in the console.
7. Given the values in the code, identify if the values of the following variable are divisible by 8.
  Use a modulo operator to identify the divisibility of the number to 8.
  Save the result of the operation in an appropriately named variable.
  Log the value of the remainder in the console.
  Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8.
  Log a message in the console if the number is divisible by 8.
  Log the value of isDivisibleBy8 in the console.
8. Given the values below, identify if the values of the following variable are divisible by 4.
  Use a modulo operator to identify the divisibility of the number to 4.
  Save the result of the operation in an appropriately named variable.
  Log the value of the remainder in the console.
  Using the strict equality operator, check if the remainder is equal to 0. 
  Save the returned value of the comparison in a variable called isDivisibleBy4. 
  Log a message in the console if the number is divisible by 4.
  Log the value of isDivisibleBy4 in the console.
9. Create a git repository named S16.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle. */